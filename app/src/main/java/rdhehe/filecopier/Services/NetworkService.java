package rdhehe.filecopier.Services;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.support.v4.app.JobIntentService;
import android.support.v4.content.LocalBroadcastManager;
import android.text.format.Formatter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import rdhehe.filecopier.Beans.File;
import rdhehe.filecopier.Beans.Network;
import rdhehe.filecopier.DAO.FilesDAO;
import rdhehe.filecopier.DAO.NetworkDAO;
import rdhehe.filecopier.Utils.ExploreNetwork_thread;
import rdhehe.filecopier.Utils.HelperUtils;
import rdhehe.filecopier.Utils.StringUtils;

public class NetworkService extends JobIntentService {
    Context context;

    // action constants
    public static final int ACTION_DISCOVERNETWORKS = 1;
    public static final int ACTION_DISCOVERFOLDERS = 2;
    public static final int ACTION_READFILETOSTR = 3;

    // thread related
    public static final int IP_INTERVAL = 10;
    public static final int IP_UPPERLIMIT = 260;
    public static boolean isRun = false;

    // broadcast related
    public static final String networkDiscovered_intentBroadcastName = "filecopier.networkdiscovered.intentbroadcastname";
    public static final String filesDiscovered_intentBroadcastName = "filecopier.filessdiscovered.intentbroadcastname";
    public static final String readFileToStr_intentBroadcastName = "filecopier.filereadtostr.intentbroadcastname";

    // constructor
    public NetworkService(){
        this.context = this;
    }
    public NetworkService(Context context){
        this.context = context;
    }

    // read file content to string
    public String readFileToString(String ip, String path, String fileName){
        Intent intent = new Intent(readFileToStr_intentBroadcastName);
        String fileContent = null;

        // smb related
        String url = "smb://" + ip + "/";
        BufferedReader br = null;

        // clean path and append to ip
        if(StringUtils.isValid(path)){
            // remove double /
            path = path.charAt(0)=='/' ? path.substring(1) : path;

            // add / at the end if not present
            path += path.charAt(path.length()-1)!='/' ? "/" : "";

            // append path and file name to url
            url += path + fileName + "/";
        }

        try {
            // read content of the file
            br = new BufferedReader(new InputStreamReader(new SmbFileInputStream(url)));
            fileContent = br.readLine();
        } catch (UnknownHostException e) {
            HelperUtils.log("UnknownHostException :: NetworkService :: readFileToString()");
        } catch (SmbException e) {
            HelperUtils.log("SMBException :: NetworkService :: readFileToString()");
        } catch (MalformedURLException e) {
            HelperUtils.log("MalformedURLException :: NetworkService :: readFileToString()");
        } catch (IOException e) {
            HelperUtils.log("IOException :: NetworkService :: readFileToString()");
        } finally {
            try {
                if(br != null)
                    br.close();
            } catch (IOException e) {
                HelperUtils.log("IOException :: NetworkService :: readFileToString() :: finallyBlock");
            }
        }

        // send file content as broadcast
        intent.putExtra("fileContent", fileContent);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        return fileContent;
    }

    // get file listing from shared IP on network
    public ArrayList<File> getFileList(String ip, String path){
        ArrayList<File> fileList = null;

        // smb related
        String url = "smb://" + ip + "/";
        SmbFile[] files;
        SmbFile smbFile;

        // clean path and append to ip
        if(StringUtils.isValid(path)){
            // remove double /
            path = path.charAt(0)=='/' ? path.substring(1) : path;

            // add / at the end if not present
            path += path.charAt(path.length()-1)!='/' ? "/" : "";

            // append path to url
            url += path;
        }

        try {
            // get file list
            smbFile = new SmbFile(url);
            files = smbFile.listFiles();

            // parse through list
            for(int x=0; files!=null && x<files.length; x++){
                // initialize array and add
                if(fileList == null)
                    fileList = new ArrayList<File>();
                fileList.add(new File(ip, path, files[x].getName(), files[x].isDirectory() ? File.FILETYPE_FOLDER : File.FILETYPE_FILE));
            }
        } catch (MalformedURLException e) {
            HelperUtils.log("MalformedURLException :: NetworkService :: getFilelist()");
        } catch (SmbException e) {
            HelperUtils.log("SMBException :: NetworkService :: getFileList()");
        }

        return fileList;
    }

    // discover computers on the network
    public void discoverComputersOnNetworks(){
        String selfIPAddress = Formatter.formatIpAddress(((WifiManager)context.getSystemService(WIFI_SERVICE)).getConnectionInfo().getIpAddress());
        String subnet = selfIPAddress.substring(0, selfIPAddress.lastIndexOf("."));
        ArrayList<Network> currentNetworks_arr = new NetworkDAO(context).getNetworks(false);
        HashMap<String, Network> currentNetworks_hash = new HashMap<String, Network>();
        String ip;
        boolean isReachable;

        // parse through the current networks and check if they are still reachable
        for(int x=0; currentNetworks_arr!=null && x<currentNetworks_arr.size(); x++){
            ip = currentNetworks_arr.get(x).getIp();
            currentNetworks_hash.put(ip, currentNetworks_arr.get(x));

            // get reachable status
            isReachable = ExploreNetwork_thread.isIPAddressReachable(ip);

            // update dB and (send broadcast if reachable)
            if(isReachable){
                new NetworkDAO(context).updateStatus(ip, getFileList(ip, null), isReachable);
                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(networkDiscovered_intentBroadcastName));
            } else
                new NetworkDAO(context).updateStatus(ip, null, isReachable);
        }

        // start threads for network discovery
        isRun = true;
        for(int x=0; x<IP_UPPERLIMIT; x+=IP_INTERVAL)
            new Thread(new ExploreNetwork_thread(x, (x+IP_INTERVAL), subnet, selfIPAddress, currentNetworks_hash, context)).start();
    }

    // discover folders on an ip
    public void discoverFolders(String ip, String path){
        Intent intent = new Intent(filesDiscovered_intentBroadcastName);
        ArrayList<File> files = getFileList(ip, path);

        // parse through files and insert into dB if doesn't contain $ (to remove print$ etc)
        for(int x=0; files!=null && x<files.size(); x++)
            if(!files.get(x).getFileName().contains("$"))
                new FilesDAO(context).insertFile(ip, path, files.get(x).getFileName(), files.get(x).getFileType());

        // send local broadcast
        intent.putExtra("ip", ip);
        intent.putExtra("path", path);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    @Override
    protected void onHandleWork(Intent intent) {
        int actionId;

        // branch off based on action
        actionId = intent.getIntExtra("actionId", 0);
        switch(actionId){
            case ACTION_DISCOVERNETWORKS:
                discoverComputersOnNetworks();
                break;

            case ACTION_DISCOVERFOLDERS:
                discoverFolders(intent.getStringExtra("ip"), intent.getStringExtra("path"));
                break;

            case ACTION_READFILETOSTR:
                readFileToString(intent.getStringExtra("ip"), intent.getStringExtra("path"), intent.getStringExtra("fileName"));
                break;
        }
    }
}