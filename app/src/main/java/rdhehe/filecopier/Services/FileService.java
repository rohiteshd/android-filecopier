package rdhehe.filecopier.Services;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v4.app.JobIntentService;
import android.support.v4.content.LocalBroadcastManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import rdhehe.filecopier.Beans.File;
import rdhehe.filecopier.Beans.StitchFile;
import rdhehe.filecopier.DAO.StitchFilesDAO;
import rdhehe.filecopier.Utils.Constants;
import rdhehe.filecopier.Utils.HelperUtils;
import rdhehe.filecopier.Utils.NotificationUtils;
import rdhehe.filecopier.Utils.StringUtils;

public class FileService extends JobIntentService{
    String ip, path;
    boolean isSplitFileOrFolder;
    Context context;

    public static final int COPYFILES_LOOP = 3;

    // action constants
    public static final int ACTION_COPYFILES = 1;
    public static final int ACTION_COPYSINGLEFILE = 2;

    // broadcast related
    public static final String fileCopy_intentBroadcastName = "filecopier.filecopied.intentbroadcastname";
    public static final String fileCopyProgress_intentBroadcastName = "filecopier.filecopyprogress.intentbroadcastname";
    public static final String fileStitched_intentBroadcastName = "filecopier.filestitched.intentbroadcastname";

    // file related
    public static final int FILE_WRITEBYTESIZE = 102400; // 100 kB each byte grab

    // constructor
    public FileService(){
        this.context = this;
    }

    // get storage location
    private java.io.File getExternalStorageDirectory(){
        return Environment.getExternalStorageDirectory();
    }

    // create stitch files array from file details json
    private ArrayList<StitchFile> getStitchFilesFromJSONFile(){
        ArrayList<StitchFile> files = null;
        String fileName;
        boolean isFileOrFolder;

        // json related
        String jsonStr = new NetworkService(context).readFileToString(ip, path, Constants.STITCHER_SPLITFILELIST_FILENAME);
        JSONObject jsonMain;
        JSONArray jsonList = new JSONArray();

        try {
            // get json
            jsonMain = new JSONObject(jsonStr);

            // get file list or folder list based on flag
            isFileOrFolder = jsonMain.getBoolean("isFileOrFolder");
            if(isFileOrFolder) {
                jsonList = jsonMain.getJSONArray("fileList");

                // parse through the files
                for(int x=0; jsonList!=null && x<jsonList.length(); x++){
                    // add to stitched files
                    if(files == null)
                        files = new ArrayList<StitchFile>();
                    files.add(new StitchFile(ip, path, jsonList.getString(x), jsonMain.getString("fileName"), StitchFile.FILESTATUS_NEW));
                }
            } else {
                // parse through the folders
                for(int x=0; x<jsonMain.getJSONArray("folderList").length(); x++) {
                    // get folder file list and details
                    jsonList = jsonMain.getJSONArray("folderList").getJSONObject(x).getJSONArray("fileList");
                    fileName = jsonMain.getJSONArray("folderList").getJSONObject(x).getString("fileName");

                    // parse through the files in the folder
                    for(int y=0; y<jsonList.length(); y++) {
                        // add to stitched files
                        if(files == null)
                            files = new ArrayList<StitchFile>();
                        files.add(new StitchFile(ip, path, jsonList.getString(y), fileName, StitchFile.FILESTATUS_NEW));
                    }
                }
            }
        } catch(JSONException e){
            HelperUtils.log("JSONException :: FileStitcherActivity :: getStitchFilesFromJSONFile()");
        }

        return files;
    }

    // copy single file
    private boolean copyFile(StitchFile stitchFile){
        boolean isDone = false;
        Intent intent;
        java.io.File folder;

        // smb related
        String path_dst, path_src, pathLog_dst;
        SmbFileInputStream sfis = null;
        FileOutputStream fos = null;
        byte[] b = new byte[FILE_WRITEBYTESIZE];
        int bytesRead, count = 0;

        // progress related
        long fileSize;
        double progress;

        // set src and dst path
        path_src = "smb://" + stitchFile.getIp() + stitchFile.getPath() + "/" + stitchFile.getFileName() + "/";
        path_dst = getExternalStorageDirectory() + java.io.File.separator + Constants.LOCALSTORAGE_FOLDER + java.io.File.separator +
                (stitchFile.getStitchedFileName().contains(".") ?
                        stitchFile.getStitchedFileName().substring(0, stitchFile.getStitchedFileName().lastIndexOf(".")) : stitchFile.getStitchedFileName());
        pathLog_dst = getExternalStorageDirectory() + java.io.File.separator + Constants.LOCALSTORAGE_FOLDER + java.io.File.separator + Constants.LOCALSTORAGE_LOGFILE;

        // update stitch file status and send broadcast
        stitchFile.setStatus(StitchFile.FILESTATUS_COPYING);
        new StitchFilesDAO(ip, path, context).updateCopyStatus(stitchFile);
        LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(fileCopy_intentBroadcastName));

        // make folder if not already done
        folder = new java.io.File(path_dst);
        if(!folder.exists() && !folder.isDirectory())
            folder.mkdirs();

        try {
            // initialize file ops
            path_dst += "/" + stitchFile.getFileName();
            sfis = new SmbFileInputStream(path_src);
            fos = new FileOutputStream(path_dst);
            fileSize = new SmbFile(path_src).length();

            // send initial progress
            intent = new Intent(fileCopyProgress_intentBroadcastName);
            intent.putExtra("progress", 0);
            intent.putExtra("file", stitchFile.getFileName());
            intent.putExtra("isNewFile", true);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

            // log
            HelperUtils.logTxt("Initializing to write " + stitchFile.getFileName(), pathLog_dst);

            // write to dst file
            while((bytesRead=sfis.read(b)) != -1) {
                fos.write(b);

                // send file copy progress
                progress = (double)(FILE_WRITEBYTESIZE * ++count)/fileSize * 100.0;
                if((int)progress % 10 == 0){
                    intent = new Intent(fileCopyProgress_intentBroadcastName);
                    intent.putExtra("progress", progress);
                    intent.putExtra("file", stitchFile.getFileName());
                    intent.putExtra("isNewFile", false);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                }
            }

            // log
            HelperUtils.logTxt("Finished writing to " + stitchFile.getFileName(), pathLog_dst);

            // set flag
            isDone = true;
        } catch (SmbException e) {
            HelperUtils.log("SMBException :: FileService :: copyFile()");
        } catch (MalformedURLException e) {
            HelperUtils.log("MalformedURLException :: FileService :: copyFile()");
        } catch (UnknownHostException e) {
            HelperUtils.log("UnknownHostException :: FileService :: copyFile()");
        } catch (IOException e) {
            HelperUtils.log("IOException :: FileService :: copyFile()");
        } finally {
            try {
                if(sfis != null)
                    sfis.close();
                if(fos != null)
                    fos.close();
            } catch (IOException e) {
                HelperUtils.log("IOException :: FileService :: copyFile() :: finally");
            }
        }

        return isDone;
    }

    // copy single file
    public void copySingleFile(String fileName, String stitchedFileName){
        boolean isDone;
        StitchFile file = new StitchFile(ip, path, fileName, stitchedFileName, StitchFile.FILESTATUS_NEW);

        // copy file and get status
        isDone = copyFile(file);

        // update file status based on copy status, update in dB and send broadcast
        file.setStatus(isDone ? StitchFile.FILESTATUS_DONE : StitchFile.FILESTATUS_NEW);
        new StitchFilesDAO(ip, path, context).updateCopyStatus(file);
        LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(fileCopy_intentBroadcastName));
    }

    // get stitched file name with path for folder copy
    /* private String getFolderFilePath(JSONObject fileListDetails_json, String fileName){
        String path = null, fileNameWithoutExtension, fileNameFromJSON;

        // json related
        JSONArray folderList_jsonArr;

        // remove extension from file name
        fileNameWithoutExtension = fileName.substring(0, fileName.lastIndexOf("."));

        try {
            // get folder list and parse through it to get the file Name
            folderList_jsonArr = fileListDetails_json.getJSONArray("folderList");
            for(int x=0; path==null && x<folderList_jsonArr.length(); x++){
                fileNameFromJSON = folderList_jsonArr.getJSONObject(x).getJSONArray("fileList").getString(0);

                // compare if same folder is located
                if(fileNameFromJSON.substring(0, fileNameFromJSON.lastIndexOf(".")).compareToIgnoreCase(fileNameWithoutExtension) == 0)
                    path = fileListDetails_json.getString("folderName") + java.io.File.separator + folderList_jsonArr.getJSONObject(x).getString("fileName");
            }
        } catch(JSONException e){
            HelperUtils.log("JSONException :: FileService :: getFolderFilePath()");
        }

        return path;
    }
    */

    // copy folder
    public void copyFolder(String stitchedFolderName){
        ArrayList<StitchFile> stitchFiles = new StitchFilesDAO(ip, path, context).getStitchFiles();
        boolean isDone;

        // loop related
        int loop = 0;
        boolean isFileCopied;

        // insert file list to dB if not already available
        if(stitchFiles == null)
            new StitchFilesDAO(ip, path, context).insertStitchFiles((stitchFiles = getStitchFilesFromJSONFile()));

        // multiple loops to copying files, to ensure no missed files
        do {
            isFileCopied = false;

            // parse through the files and start copying
            for(int x=0; stitchFiles!=null && x<stitchFiles.size(); x++) {
                // copy file if not copied
                if (stitchFiles.get(x).getStatus() == StitchFile.FILESTATUS_NEW) {
                    isDone = copyFile(stitchFiles.get(x));
                    isFileCopied = true;

                    // update file status based on copy status, update in dB and send broadcast
                    stitchFiles.get(x).setStatus(isDone ? StitchFile.FILESTATUS_DONE : StitchFile.FILESTATUS_NEW);
                    new StitchFilesDAO(ip, path, context).updateCopyStatus(stitchFiles.get(x));
                    LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(fileCopy_intentBroadcastName));
                }
            }
        } while(isFileCopied || loop++<COPYFILES_LOOP);

        // stitch files
        stitchFiles(stitchedFolderName);
    }

    // copy multiple files
    public void copyFiles(String stitchedFileName){
        ArrayList<StitchFile> stitchFiles = new StitchFilesDAO(ip, path, context).getStitchFiles();
        ArrayList<File> fileList;
        boolean isDone;

        // loop related
        int loop = 0;
        boolean isFileCopied;

        // insert file list to dB if not available
        if(stitchFiles == null)
            new StitchFilesDAO(ip, path, context).insertStitchFiles((stitchFiles=getStitchFilesFromJSONFile()));

        // multiple loops to copying files, to ensure no missed files
        do {
            isFileCopied = false;

            // parse through the files and start copying
            for(int x=0; stitchFiles!=null && x<stitchFiles.size(); x++) {
                // copy file if not copied
                if (stitchFiles.get(x).getStatus() == StitchFile.FILESTATUS_NEW) {
                    isDone = copyFile(stitchFiles.get(x));
                    isFileCopied = true;

                    // update file status based on copy status, update in dB and send broadcast
                    stitchFiles.get(x).setStatus(isDone ? StitchFile.FILESTATUS_DONE : StitchFile.FILESTATUS_NEW);
                    new StitchFilesDAO(ip, path, context).updateCopyStatus(stitchFiles.get(x));
                    LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(fileCopy_intentBroadcastName));
                }
            }
        } while(isFileCopied || loop++<COPYFILES_LOOP);

        // stitch files
        stitchFiles(stitchedFileName);
    }

    // stitch single file
    private void stitchSingleFile(String stitchedFileName, String folderName, JSONArray fileList_jsonArr){
        // file related
        boolean isValid = false, isFileExist;
        FileInputStream fis = null;
        FileOutputStream fos = null;
        java.io.File file;
        byte[] b = new byte[FILE_WRITEBYTESIZE];
        int bytesRead;
        String filePath_input;
        String filePath_output = getExternalStorageDirectory() + java.io.File.separator + Constants.LOCALSTORAGE_FOLDER + java.io.File.separator +
                (StringUtils.isValid(folderName) ? folderName : stitchedFileName.substring(0, stitchedFileName.lastIndexOf("."))) +
                java.io.File.separator;

        // log related
        String pathLog_dst = getExternalStorageDirectory() + java.io.File.separator + Constants.LOCALSTORAGE_FOLDER + java.io.File.separator +
                Constants.LOCALSTORAGE_LOGFILE;

        // log
        HelperUtils.logTxt("\nStitching filepath output "+filePath_output, pathLog_dst);

        try {
            // parse through the split files
            for(int x=0; x<fileList_jsonArr.length(); x++){
                // set file path input and check if file exists
                filePath_input = getExternalStorageDirectory() + java.io.File.separator + Constants.LOCALSTORAGE_FOLDER + java.io.File.separator +
                        fileList_jsonArr.getString(x).substring(0, fileList_jsonArr.getString(x).lastIndexOf(".")) + java.io.File.separator;
                isFileExist = new java.io.File(filePath_input+fileList_jsonArr.getString(x)).exists();

                // log
                HelperUtils.logTxt("Stitching filepath input "+filePath_input, pathLog_dst);
                HelperUtils.logTxt("Stitching file input "+filePath_input+fileList_jsonArr.getString(x), pathLog_dst);
                HelperUtils.logTxt("isFileExist "+isFileExist, pathLog_dst);

                // proceed only if input file exists
                if(isFileExist){
                    // create dst directory and file output stream
                    if(x == 0) {
                        if(!new java.io.File(filePath_output).isDirectory())
                            new java.io.File(filePath_output).mkdir();
                        fos = new FileOutputStream(filePath_output + stitchedFileName);
                    }

                    // log
                    HelperUtils.logTxt("Initialize Stitching "+fileList_jsonArr.getString(x), pathLog_dst);

                    // read from the split file and write to the dst file
                    fis = new FileInputStream(filePath_input + fileList_jsonArr.getString(x));
                    while((bytesRead=fis.read(b)) != -1)
                        fos.write(b);

                    // close split file
                    fis.close();

                    // set valid flag
                    isValid = true;

                    // log
                    HelperUtils.logTxt("Completed Stitching "+fileList_jsonArr.getString(x), pathLog_dst);
                }
            }

            // proceed if valid
            if(isValid){
                // close stitched file
                fos.close(); fos = null;

                // log
                HelperUtils.logTxt("Completed Stitching Full File "+stitchedFileName, pathLog_dst);

                // parse through the split files and delete
                for(int x=0; x<fileList_jsonArr.length(); x++) {
                    filePath_input = getExternalStorageDirectory() + java.io.File.separator + Constants.LOCALSTORAGE_FOLDER + java.io.File.separator +
                            fileList_jsonArr.getString(x).substring(0, fileList_jsonArr.getString(x).lastIndexOf(".")) + java.io.File.separator;
                    if((file=new java.io.File(filePath_input + fileList_jsonArr.getString(x))).exists())
                        file.delete();
                }

                // delete split file list if not folder
                if(!StringUtils.isValid(folderName) && (file=new java.io.File(filePath_output + Constants.STITCHER_SPLITFILELIST_FILENAME)).exists())
                    file.delete();

                // update stitch location, send broadcast and notification
                new StitchFilesDAO(ip, path, context).updateLocationStitchStatus(true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(fileStitched_intentBroadcastName));
                new NotificationUtils(NotificationUtils.stitchingComplete_notificationId, context).createStitchingCompleteNotification(ip, path, stitchedFileName);
            }
        } catch (FileNotFoundException e) {
            HelperUtils.log("FileNotFoundException :: FileService :: stitchSingleFile()");

            // log
            HelperUtils.logTxt("FileNotFoundException : "+e.toString(), pathLog_dst);
        } catch (IOException e) {
            HelperUtils.log("IOException :: FileService :: stitchSingleFile()");

            // log
            HelperUtils.logTxt("IOException", pathLog_dst);
        } catch (JSONException e) {
            HelperUtils.log("JSONException :: FileService :: stitchSingleFile()");

            // log
            HelperUtils.logTxt("JSONException", pathLog_dst);
        } finally {
            try {
                if(fis != null)
                    fis.close();
                if(fos != null)
                    fos.close();
            } catch(IOException e){
                HelperUtils.log("IOException :: FileService :: stitchSingleFile() :: finally");

                // log
                HelperUtils.logTxt("IOException In finally", pathLog_dst);
            }
        }
    }

    // stitch files
    public void stitchFiles(String stitchedFileName){
        boolean isAllFilesCopied = false, isSplitFileOrFolder;
        ArrayList<StitchFile> stitchFiles = new StitchFilesDAO(ip, path, context).getStitchFiles();
        String folderName;

        // json related
        JSONObject fileListDetails_json;
        JSONArray fileList_jsonArr;

        // file related
        java.io.File fileListFile_local;
        String pathLog_dst = getExternalStorageDirectory() + java.io.File.separator + Constants.LOCALSTORAGE_FOLDER + java.io.File.separator +
                Constants.LOCALSTORAGE_LOGFILE;

        // parse through all stitch files and check if all copied
        for(int x=0; stitchFiles!=null && x<stitchFiles.size(); x++)
            isAllFilesCopied = stitchFiles.get(x).getStatus()==StitchFile.FILESTATUS_DONE && (x==0 ? true : isAllFilesCopied);

        // log
        HelperUtils.logTxt("\nIsAllFilesCopied Status "+isAllFilesCopied, pathLog_dst);

        // proceed if all files copied
        if(isAllFilesCopied)
            try {
                // get file list json, file or folder flag, appropriate list and folder name
                fileListDetails_json = new JSONObject(new NetworkService(context).readFileToString(ip, path, Constants.STITCHER_SPLITFILELIST_FILENAME));
                isSplitFileOrFolder = fileListDetails_json.getBoolean("isFileOrFolder");
                fileList_jsonArr = isSplitFileOrFolder ? fileListDetails_json.getJSONArray("fileList") : fileListDetails_json.getJSONArray("folderList");
                folderName = isSplitFileOrFolder ? "" : fileListDetails_json.getString("folderName");

                // stitch file or folder
                if(isSplitFileOrFolder)
                    stitchSingleFile(stitchedFileName, null, fileList_jsonArr);
                else {
                    // parse through each file in file list and stitch it up
                    for (int x = 0; x < fileList_jsonArr.length(); x++)
                        stitchSingleFile(fileList_jsonArr.getJSONObject(x).getString("fileName"), folderName, fileList_jsonArr.getJSONObject(x).getJSONArray("fileList"));

                    // delete split file list
                    fileListFile_local = new java.io.File(getExternalStorageDirectory() + java.io.File.separator + Constants.LOCALSTORAGE_FOLDER + java.io.File.separator + folderName +
                            java.io.File.separator + Constants.STITCHER_SPLITFILELIST_FILENAME);
                    if(fileListFile_local.exists())
                        fileListFile_local.delete();
                }
            } catch(JSONException e){
                HelperUtils.log("JSONException :: FileService :: stitchFiles()");
            }
    }

    @Override
    public boolean onStopCurrentWork() {
        super.onStopCurrentWork();

        // reset the copying file status
        new StitchFilesDAO(ip, path, context).resetCopyingFilesToNew();

        return true;
    }

    @Override
    protected void onHandleWork(Intent intent) {
       int actionId;

       // branch off based on action
       actionId = intent.getIntExtra("actionId", 0);
       switch(actionId){
           case ACTION_COPYFILES:
               // set instance variables
               ip = intent.getStringExtra("ip"); path = intent.getStringExtra("path");
               isSplitFileOrFolder = intent.getBooleanExtra("isFileOrFolder", true);

               // copy files or folders
               if(isSplitFileOrFolder)
                   copyFiles(intent.getStringExtra("stitchedFileName"));
               else
                   copyFolder(intent.getStringExtra("stitchedFileName"));
               break;

           case ACTION_COPYSINGLEFILE:
               // set instance variables
               ip = intent.getStringExtra("ip"); path = intent.getStringExtra("path");
               isSplitFileOrFolder = intent.getBooleanExtra("isFileOrFolder", true);

               // copy single file
               copySingleFile(intent.getStringExtra("fileName"), intent.getStringExtra("stitchedFileName"));
               break;
       }
    }
}