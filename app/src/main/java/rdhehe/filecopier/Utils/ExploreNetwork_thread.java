package rdhehe.filecopier.Utils;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import java.io.IOException;
import java.net.InetAddress;
import java.util.HashMap;

import rdhehe.filecopier.Beans.Network;
import rdhehe.filecopier.DAO.NetworkDAO;
import rdhehe.filecopier.Services.NetworkService;

public class ExploreNetwork_thread implements Runnable {
    int startPoint, endPoint;
    String subnet, selfAddress;
    HashMap<String, Network> currentNetworks;
    Context context;

    // constants
    public static final int CONNECTION_TIMEOUT = 3000;

    // constructor
    public ExploreNetwork_thread(int startPoint, int endPoint, String subnet, String selfAddress, HashMap<String, Network> currentNetworks, Context context){
        this.startPoint = startPoint;
        this.endPoint = endPoint;
        this.subnet = subnet;
        this.selfAddress = selfAddress;
        this.currentNetworks = currentNetworks;
        this.context = context;
    }

    @Override
    public void run() {
        String ip = "";

        // parse through all the IP addresses in the network
        for(int x=startPoint; NetworkService.isRun && x<endPoint && x<=255; x++){
            // form address
            ip = subnet + "." + x;

            // ping this address if not self
            if (ip.compareTo(selfAddress)!=0 && isIPAddressReachable(ip)){
                // insert into dB or update status if already present and send local broadcast
                if(currentNetworks.containsKey(ip))
                    new NetworkDAO(context).updateStatus(ip, new NetworkService(context).getFileList(ip, null), true);
                else
                    new NetworkDAO(context).insertNetwork(ip, new NetworkService(context).getFileList(ip, null));
                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(NetworkService.networkDiscovered_intentBroadcastName));
            }
        }
    }

    // check ip address reachable
    public static boolean isIPAddressReachable(String ip){
        boolean isReachable = false;

        try {
            isReachable = InetAddress.getByName(ip).isReachable(CONNECTION_TIMEOUT);
        } catch(IOException e){
            HelperUtils.log("IOException :: ExploreNetwork_thread :: isIPaddressReachable()");
        }

        return isReachable;
    }
}