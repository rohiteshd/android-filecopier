package rdhehe.filecopier.Utils;

public class Constants {
    // file stitcher
    public static final String STITCHER_SPLITFILELIST_FILENAME = "FileDetails_rdFileSplitter.txt";

    // local storage
    public static final String LOCALSTORAGE_FOLDER = "RD";
    public static final String LOCALSTORAGE_LOGFILE = "log.txt";

	// shared preferences
	public static final String SHARED_PREF_FILENAME_PERMANENT = "FileCopier_PREF_DATA_PERM";

    // service constants
    public static final int SERVICE_NETWORK = 100;
    public static final int SERVICE_FILES = 200;
}