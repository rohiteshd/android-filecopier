package rdhehe.filecopier.Utils;

import java.util.Calendar;

public class TimeUtils {
    // get time stamp
    public static String getTimestamp(){
        Calendar cal = Calendar.getInstance();
        int hour = cal.get(Calendar.HOUR_OF_DAY), minute = cal.get(Calendar.MINUTE);
        String hour_str = hour<10 ? "0"+Integer.toString(hour) : Integer.toString(hour);
        String minute_str = minute<10 ? "0"+Integer.toString(minute) : Integer.toString(minute);
        String timestamp = hour_str + minute_str;

        return timestamp;
    }
}