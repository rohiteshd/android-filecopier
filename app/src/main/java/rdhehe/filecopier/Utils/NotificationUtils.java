package rdhehe.filecopier.Utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import rdhehe.filecopier.Activities.FileStitcherActivity;
import rdhehe.filecopier.Activities.NetworkActivity;
import rdhehe.filecopier.R;

public class NotificationUtils {
    Context context;
    Intent newIntent;
    PendingIntent pendingIntent;
    NotificationManager notificationManager;
    NotificationCompat.Builder notificationBuilder;
    int notificationId;

    // notify related
    public static final int stitchingComplete_notificationId = 1;

    // constructor
    public NotificationUtils(int notificationId, Context context){
        this.context = context;
        this.notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        this.notificationBuilder = new NotificationCompat.Builder(context);
        this.newIntent = new Intent();
        this.notificationId = notificationId;
    }

    // stitching complete notification
    public void createStitchingCompleteNotification(String ip, String path, String stitchedFileName){
        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
        String notificationStr = "Stitching Of " + stitchedFileName + " Complete!";

        // set the pending intent
        newIntent = new Intent(context, FileStitcherActivity.class);
        newIntent.putExtra("ip", ip);
        newIntent.putExtra("path", path);
        newIntent.putExtra("stitchedFileName", stitchedFileName);
        taskStackBuilder.addNextIntent(newIntent);
        pendingIntent = taskStackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        // create the notification
        notificationBuilder
                .setSmallIcon(R.drawable.notification)
                .setContentTitle("Stitching Complete!")
                .setContentText(notificationStr)
                .setContentIntent(pendingIntent)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setAutoCancel(true);

        // show the notification
        notificationManager.notify(stitchingComplete_notificationId, notificationBuilder.build());
    }

    // cancel a notification
    public void cancelNotification(){
        notificationManager.cancel(notificationId);
    }
}