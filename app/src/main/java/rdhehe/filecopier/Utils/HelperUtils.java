package rdhehe.filecopier.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import jcifs.smb.SmbFileInputStream;

public class HelperUtils {
	public static final boolean isLog = true;
	public static final String TAG = "RDHEHETag";

	// show a toast message
	public static void showToastMessage(Context context, String message, int duration) {
		Toast toast;

		// instantiate toast
		duration = duration == -1 ? Toast.LENGTH_LONG : duration;
		toast = Toast.makeText(context, message, duration);
		toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
		toast.show();
	}

	public static void setSharedPreferencesString(String sfFileName, String key, String value, Context context) {
		SharedPreferences settings = context.getSharedPreferences(sfFileName, 0);
		SharedPreferences.Editor settingsEditor;
		settingsEditor = settings.edit();
		settingsEditor.putString(key, value);
		settingsEditor.commit();
	}

	public static String getSharedPreferencesString(String sfFileName, String sharedPref_key, Context context) {
		String sharedPref_value;

		// get the shared reference value
		sharedPref_value = context.getSharedPreferences(sfFileName, 0).getString(sharedPref_key, null);

		// special cases for null values
		if (sharedPref_value == null)
			switch (sharedPref_key) {
				/* case Constants.EVENT_CURRENTEVENT:
					sharedPref_value = "";
					break;
				*/
			}

		return sharedPref_value;
	}

	public static Thread performOnBackgroundThread(final Runnable runnable, final Context ctx) {
		final Thread t = new Thread() {
			@Override
			public void run() {
				try {
					runnable.run();
				} catch (Exception e) {
				}
			}
		};
		t.start();

		return t;
	}

	public static int getAPIlevel() {
		int apilevel = android.os.Build.VERSION.SDK_INT;
		return apilevel;
	}

	// log (only for debugging
	public static void log(String logMessage) {
		if (isLog)
			Log.v(TAG, logMessage);
	}

	// log into txt file
	public static void logTxt(String logTxt, String logFilePath) {
		FileOutputStream fos = null;
		byte[] b;

		try {
			// append timestamp
			logTxt += " - " + TimeUtils.getTimestamp();
			
			// initialize file ops
			fos = new FileOutputStream(logFilePath, true);
			fos.write((logTxt+"\n").getBytes());
		} catch (FileNotFoundException e) {
			HelperUtils.log("FileNotFoundException :: HelperUtils :: logTxt()");
		} catch (IOException e) {
			HelperUtils.log("IOException :: HelperUtils :: logTxt()");
		}
	}
}