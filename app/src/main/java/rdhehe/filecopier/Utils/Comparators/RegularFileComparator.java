package rdhehe.filecopier.Utils.Comparators;

import java.util.Comparator;

import rdhehe.filecopier.Beans.File;

// regular file and folder comparator
public class RegularFileComparator implements Comparator<File> {
    @Override
    public int compare(File file0, File file1) {
        int retVal = 0;

        // folders over files
        if(file0.getFileType()==File.FILETYPE_FOLDER && file1.getFileType()==File.FILETYPE_FILE)
            retVal = -1;
        if(retVal==0 && file0.getFileType()==File.FILETYPE_FILE && file1.getFileType()==File.FILETYPE_FOLDER)
            retVal = 1;

        // if both are same type, then alphabetical
        if(retVal == 0)
            retVal = file0.getFileName().compareToIgnoreCase(file1.getFileName()) > 0 ? 1 : -1;

        return retVal;
    }
}