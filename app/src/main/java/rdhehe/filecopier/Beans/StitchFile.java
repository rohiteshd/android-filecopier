package rdhehe.filecopier.Beans;

public class StitchFile {
    String ip;
    String path;
    String fileName;
    String stitchedFileName;
    int status;

    // file status constants
    public static final int FILESTATUS_COPYING = 1;
    public static final int FILESTATUS_NEW = 2;
    public static final int FILESTATUS_DONE = 3;

    // constructor
    public StitchFile(String ip, String path, String fileName, String stitchedFileName, int status){
        this.ip = ip;
        this.path = path;
        this.fileName = fileName;
        this.stitchedFileName = stitchedFileName;
        this.status = status;
    }

    public String getIp() {
        return ip;
    }
    public String getPath() {
        return path;
    }
    public String getFileName() {
        return fileName;
    }
    public int getStatus() {
        return status;
    }
    public String getStitchedFileName() {
        return stitchedFileName;
    }
    public void setStatus(int status) {
        this.status = status;
    }
}