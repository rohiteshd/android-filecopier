package rdhehe.filecopier.Beans;

public class Network {
    String ip;
    String sharedFiles;
    boolean status;

    // constructor
    public Network(String ip, String sharedFiles, boolean status){
        this.ip = ip;
        this.sharedFiles = sharedFiles;
        this.status = status;
    }

    public String getIp() {
        return ip;
    }
    public boolean isStatus() {
        return status;
    }
    public String getSharedFiles() {
        return sharedFiles;
    }
}