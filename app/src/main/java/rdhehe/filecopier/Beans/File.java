package rdhehe.filecopier.Beans;

public class File {
    String ip;
    String path;
    String fileName;
    int fileType;

    // constants
    public static final int FILETYPE_FILE = 1;
    public static final int FILETYPE_FOLDER = 2;

    // constructor
    public File(String ip, String path, String fileName, int fileType){
        this.ip = ip;
        this.path = path;
        this.fileName = fileName;
        this.fileType = fileType;
    }

    public String getIp() {
        return ip;
    }
    public String getPath() {
        return path;
    }
    public String getFileName() {
        return fileName;
    }
    public int getFileType() {
        return fileType;
    }
}