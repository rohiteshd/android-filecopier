package rdhehe.filecopier.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import rdhehe.filecopier.Adapters.NetworkAdapter;
import rdhehe.filecopier.DAO.NetworkDAO;
import rdhehe.filecopier.R;
import rdhehe.filecopier.Services.NetworkService;
import rdhehe.filecopier.Utils.Constants;

public class NetworkActivity extends BaseActivity {
    Context context;

    RecyclerView networks_recyclerView;

    BroadcastReceiver networkDiscovered_broadcastReceiver;

    // constructor
    public NetworkActivity(){
        super(BaseActivity.ACTIVIY_NETWORK);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;

        LinearLayoutManager linearLayoutManager;

        // setup view
        setContentView(R.layout.networks);
        Toolbar toolbar = (Toolbar) findViewById(R.id.appToolbar);
        setSupportActionBar(toolbar);

        // setup recyclerview
        networks_recyclerView = (RecyclerView)findViewById(R.id.networks_recyclerView);
        linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        networks_recyclerView.setLayoutManager(linearLayoutManager);

        // reset all old networks
        new NetworkDAO(context).resetAllNetworks();

        // discover networks
        Intent intent = new Intent(context, NetworkService.class);
        intent.putExtra("actionId", NetworkService.ACTION_DISCOVERNETWORKS);
        NetworkService.enqueueWork(context, NetworkService.class, Constants.SERVICE_NETWORK, intent);
    }

    @Override
    public void onResume(){
        super.onResume();

        // show networks
        showNetworks();
    }

    @Override
    public void onStart(){
        super.onStart();

        // network discovered broadcast receiver
        networkDiscovered_broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // refresh networks
                showNetworks();
            }
        };
        LocalBroadcastManager.getInstance(context).registerReceiver(networkDiscovered_broadcastReceiver, new IntentFilter(NetworkService.networkDiscovered_intentBroadcastName));
    }

    @Override
    public void onStop(){
        super.onStop();

        // stop explore network thread
        NetworkService.isRun = false;

        // unregister broadcast receivers
        LocalBroadcastManager.getInstance(context).unregisterReceiver(networkDiscovered_broadcastReceiver);
    }

    // show networks
    public void showNetworks(){
        networks_recyclerView.setAdapter(new NetworkAdapter(new NetworkDAO(context).getNetworks(true), context));
    }
}