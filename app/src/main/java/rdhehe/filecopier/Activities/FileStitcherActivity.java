package rdhehe.filecopier.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import rdhehe.filecopier.Adapters.StitcherFilesAdapter;
import rdhehe.filecopier.Beans.StitchFile;
import rdhehe.filecopier.DAO.StitchFilesDAO;
import rdhehe.filecopier.R;
import rdhehe.filecopier.Services.FileService;
import rdhehe.filecopier.Utils.Comparators.StitchFileComparator;
import rdhehe.filecopier.Utils.Constants;
import rdhehe.filecopier.Utils.HelperUtils;

public class FileStitcherActivity extends BaseActivity {
    String ip, path, stitchedFileName;
    boolean isSplitFileOrFolder;
    boolean isSorting = false;

    Context context;

    RecyclerView stitchFiles_recyclerView;
    TextView singleFileProgress_textView, allFileProgress_textView;

    // permission related
    boolean isExternalWritePermission;

    BroadcastReceiver fileCopy_broadcastReceiver, fileCopyProgress_broadcastReceiver, fileStitched_broadcastReceiver;

    // constructor
    public FileStitcherActivity(){
        super(ACTIVIY_FILESTITCHER);
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        context = this;

        LinearLayoutManager linearLayoutManager;

        // setup view
        setContentView(R.layout.files);
        Toolbar toolbar = (Toolbar) findViewById(R.id.appToolbar);
        setSupportActionBar(toolbar);

        // setup recyclerview
        stitchFiles_recyclerView = (RecyclerView)findViewById(R.id.files_recyclerView);
        linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        stitchFiles_recyclerView.setLayoutManager(linearLayoutManager);
        stitchFiles_recyclerView.setAdapter(new StitcherFilesAdapter(null, ip, path, context));

        // make progress textviews visible
        singleFileProgress_textView = (TextView)findViewById(R.id.singleFileProgressTextView);
        allFileProgress_textView = (TextView)findViewById(R.id.allFileProgressTextView);
        findViewById(R.id.singleFileProgressTextView).setVisibility(View.VISIBLE);
        findViewById(R.id.allFileProgressTextView).setVisibility(View.VISIBLE);

        // set the IP and path of this network
        ip = getIntent().getStringExtra("ip");
        path = getIntent().getStringExtra("path");
        stitchedFileName = getIntent().getStringExtra("stitchedFileName");
        isSplitFileOrFolder = getIntent().getBooleanExtra("isFileOrFolder", true);

        // get external write permission and start file copy
        isExternalWritePermission = getPermission(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_EXTERNALWRITE, new boolean[]{true}, context)[0];
        copyFiles();
    }

    @Override
    public void onResume(){
        super.onResume();

        // show stitch files
        showFiles();
    }

    @Override
    public void onStart(){
        super.onStart();

        // file copy status update broadcast receiver
        fileCopy_broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // refresh stitched file list
                showFiles();
            }
        };
        LocalBroadcastManager.getInstance(context).registerReceiver(fileCopy_broadcastReceiver, new IntentFilter(FileService.fileCopy_intentBroadcastName));

        // files stitched update broadcast receiver
        fileStitched_broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                showFiles();
            }
        };
        LocalBroadcastManager.getInstance(context).registerReceiver(fileStitched_broadcastReceiver, new IntentFilter(FileService.fileStitched_intentBroadcastName));

        // file copy progress broadcast receiver
        fileCopyProgress_broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ArrayList<StitchFile> files = new StitchFilesDAO(ip, path, context).getStitchFiles();
                int totalFiles = files!=null ? files.size() : 0, fileIndex = 0;
                double progress = intent.getDoubleExtra("progress", 0.0);
                String fileName = intent.getStringExtra("file");
                boolean isNewFile = intent.getBooleanExtra("isNewFile", false);

                // update single file progress
                singleFileProgress_textView.setText(progress + "% of " + fileName);

                // parse through all files and get file index
                for(int x=0; files!=null && x<files.size(); x++)
                    if(files.get(x).getFileName().compareTo(fileName) == 0){
                        fileIndex = x;
                        break;
                    }

                // update multiple file progress
                allFileProgress_textView.setText(fileIndex + "/" + totalFiles + " Files copied!");
            }
        };
        LocalBroadcastManager.getInstance(context).registerReceiver(fileCopyProgress_broadcastReceiver, new IntentFilter(FileService.fileCopyProgress_intentBroadcastName));
    }

    @Override
    public void onStop(){
        super.onStop();

        // unregister broadcast receivers
        LocalBroadcastManager.getInstance(context).unregisterReceiver(fileCopy_broadcastReceiver);
        LocalBroadcastManager.getInstance(context).unregisterReceiver(fileCopyProgress_broadcastReceiver);
        LocalBroadcastManager.getInstance(context).unregisterReceiver(fileStitched_broadcastReceiver);
    }

    // show files
    public void showFiles(){
        boolean isAllCopied = false;
        ArrayList<StitchFile> files = new StitchFilesDAO(ip, path, context).getStitchFiles();
        StitcherFilesAdapter adapter;
        AlertDialog.Builder alertDialogBuilder;

        // parse through all files and check if copied
        for(int x=0; files!=null && x<files.size(); x++)
            isAllCopied = files.get(x).getStatus()==StitchFile.FILESTATUS_DONE && (x==0 ? true : isAllCopied);

        // stitched
        if(new StitchFilesDAO(ip, path, context).isLocationStitched()){
            // create the alert dialog
            alertDialogBuilder = new AlertDialog.Builder(context);
            alertDialogBuilder.setTitle("Complete")
                    .setMessage("All files have been copied and stitched!")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i){
                            dialogInterface.cancel();
                        }
                    })
                    .create().show();
        } else {
            // sort files if not null and not already sorting
            if(files!=null && !isSorting) {
                try {
                    isSorting = true;
                    Collections.sort(files, new StitchFileComparator());
                } catch(IllegalArgumentException e){
                    HelperUtils.log("Illegal Argument Exception");
                }
                isSorting = false;
            }

            // update adapter
            adapter = (StitcherFilesAdapter)stitchFiles_recyclerView.getAdapter();
            if(adapter != null) {
                adapter.setFiles(files);
                adapter.notifyDataSetChanged();
            }
        }
    }

    // copy files
    public void copyFiles(){
        boolean isStitched = new StitchFilesDAO(ip, path, context).isLocationStitched();
        Intent intent = new Intent(context, FileService.class);

        // start service to copy files if permission granted
        if(isExternalWritePermission && !isStitched){
            intent.putExtra("actionId", FileService.ACTION_COPYFILES);
            intent.putExtra("ip", ip);
            intent.putExtra("path", path);
            intent.putExtra("isFileOrFolder", isSplitFileOrFolder);
            intent.putExtra("stitchedFileName", stitchedFileName);
            FileService.enqueueWork(context, FileService.class, Constants.SERVICE_FILES, intent);
        } else
            HelperUtils.showToastMessage(context, "Please provide permission to app to proceed!", -1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        // parse through permissions and check for granted
        for(int x=0; x<permissions.length; x++)
            switch(permissions[x]){
                case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                    isExternalWritePermission = grantResults[x] == PackageManager.PERMISSION_GRANTED;
                    copyFiles();
                    break;
            }
    }
}