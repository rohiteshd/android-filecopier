package rdhehe.filecopier.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {
    int activityId;

    // activity constants
    public static final int ACTIVIY_NETWORK = 1;
    public static final int ACTIVIY_FILES = 2;
    public static final int ACTIVIY_FILESTITCHER = 3;

    // permission constants
    public static final int PERMISSION_EXTERNALREAD = 1;
    public static final int PERMISSION_EXTERNALWRITE = 2;

    // constructor
    public BaseActivity(int activityId){
        this.activityId = activityId;
    }

    // get permission
    public static boolean[] getPermission(String[] permissions, int requestCode, boolean[] isAskPermissions, Context context){
        boolean[] isPermissions = new boolean[permissions.length];

        // check if permissions available
        for(int x=0; permissions!=null && x<permissions.length; x++)
            isPermissions[x] = ContextCompat.checkSelfPermission(context, permissions[x])==PackageManager.PERMISSION_GRANTED;

        // ask for permission if not already granted
        for(int x=0; permissions!=null && x<permissions.length; x++)
            if(!isPermissions[x] && isAskPermissions[x])
                ActivityCompat.requestPermissions((Activity)context, permissions, requestCode);

        return isPermissions;
    }
}
