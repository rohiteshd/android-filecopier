package rdhehe.filecopier.Activities;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import rdhehe.filecopier.Adapters.FilesAdapter;
import rdhehe.filecopier.Beans.File;
import rdhehe.filecopier.DAO.FilesDAO;
import rdhehe.filecopier.R;
import rdhehe.filecopier.Services.NetworkService;
import rdhehe.filecopier.Utils.Comparators.RegularFileComparator;
import rdhehe.filecopier.Utils.Constants;
import rdhehe.filecopier.Utils.HelperUtils;
import rdhehe.filecopier.Utils.StringUtils;

public class FilesActivity extends BaseActivity {
    String ip, path;
    String stitchFileName;
    boolean isSplitFileOrFolder;

    Context context;

    RecyclerView files_recyclerView;

    BroadcastReceiver filesDiscovered_broadcastReceiver, isCopyFiles_broadcastReceiver;

    // constructor
    public FilesActivity(){
        super(BaseActivity.ACTIVIY_FILES);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;

        LinearLayoutManager linearLayoutManager;

        // setup view
        setContentView(R.layout.files);
        Toolbar toolbar = (Toolbar) findViewById(R.id.appToolbar);
        setSupportActionBar(toolbar);

        // setup recyclerview
        files_recyclerView = (RecyclerView)findViewById(R.id.files_recyclerView);
        linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        files_recyclerView.setLayoutManager(linearLayoutManager);

        // set the IP and path of this network
        ip = getIntent().getStringExtra("ip");
        path = getIntent().getStringExtra("path");

        // clear files dB
        new FilesDAO(context).clearFilesOfIP(ip);

        // discover folders
        Intent intent = new Intent(context, NetworkService.class);
        intent.putExtra("actionId", NetworkService.ACTION_DISCOVERFOLDERS);
        intent.putExtra("ip", ip);
        intent.putExtra("path", path);
        NetworkService.enqueueWork(context, NetworkService.class, Constants.SERVICE_NETWORK, intent);

        // show files
        showFiles();
    }

    @Override
    public void onStart(){
        super.onStart();

        // folders discovered broadcast receiver
        filesDiscovered_broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // update ip and path if available
                ip = StringUtils.isValid(intent.getStringExtra("ip")) ? intent.getStringExtra("ip") : ip;
                path = StringUtils.isValid(intent.getStringExtra("path")) ? intent.getStringExtra("path") : path;

                // refresh folders
                showFiles();
            }
        };
        LocalBroadcastManager.getInstance(context).registerReceiver(filesDiscovered_broadcastReceiver, new IntentFilter(NetworkService.filesDiscovered_intentBroadcastName));

        // get copy file name broadcast receiver
        isCopyFiles_broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String fileContent = intent.getStringExtra("fileContent");

                // pass on control
                isCopyFile(fileContent);
            }
        };
        LocalBroadcastManager.getInstance(context).registerReceiver(isCopyFiles_broadcastReceiver, new IntentFilter(NetworkService.readFileToStr_intentBroadcastName));
    }

    @Override
    public void onStop(){
        super.onStop();

        // unregister broadcast receivers
        LocalBroadcastManager.getInstance(context).unregisterReceiver(filesDiscovered_broadcastReceiver);
        LocalBroadcastManager.getInstance(context).unregisterReceiver(isCopyFiles_broadcastReceiver);
    }

    // confirm from user for copy file
    public void isCopyFile(String splitFileDetails_jsonStr){
        AlertDialog.Builder alertDialogBuilder;

        // json related
        JSONObject splitFileDetails_json;

        // proceed if file content is valid
        if(StringUtils.isValid(splitFileDetails_jsonStr)) {
            try {
                // form json of split file details and check if file or folder
                splitFileDetails_json = new JSONObject(splitFileDetails_jsonStr);
                isSplitFileOrFolder = splitFileDetails_json.getBoolean("isFileOrFolder");
                stitchFileName = isSplitFileOrFolder ? splitFileDetails_json.getString("fileName") : splitFileDetails_json.getString("folderName");

                // create the alert dialog
                alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle("Copy " + (isSplitFileOrFolder ? "File" : "Folder"))
                        .setMessage("Copy " + (isSplitFileOrFolder ? "File " : "Folder ") + stitchFileName + "?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i){
                                Intent intent = new Intent(context, FileStitcherActivity.class);

                                // pass on control to file stitcher activity
                                intent.putExtra("ip", ip);
                                intent.putExtra("path", path);
                                intent.putExtra("isFileOrFolder", isSplitFileOrFolder);
                                intent.putExtra("stitchedFileName", stitchFileName);
                                context.startActivity(intent);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        })
                        .create().show();
            } catch (JSONException e) {
                HelperUtils.log("JSONException :: FilesActivity :: isCopyFile()");
            }
        }
    }

    // check for file splitter folder
    private void isFileSplitterFolder(ArrayList<File> files){
        boolean isDone = false;
        Intent intent;

        // parse through the files and check for file splitter master file list
        for(int x=0; !isDone && files!=null && x<files.size(); x++)
            if(files.get(x).getFileType()==File.FILETYPE_FILE && files.get(x).getFileName().compareTo(Constants.STITCHER_SPLITFILELIST_FILENAME)==0) {
                // request for json content of file list
                intent = new Intent(context, NetworkService.class);
                intent.putExtra("actionId", NetworkService.ACTION_READFILETOSTR);
                intent.putExtra("ip", ip);
                intent.putExtra("path", path);
                intent.putExtra("fileName", Constants.STITCHER_SPLITFILELIST_FILENAME);
                NetworkService.enqueueWork(context, NetworkService.class, Constants.SERVICE_NETWORK, intent);

                // set flag
                isDone = true;
            }
    }

    // show files
    public void showFiles(){
        ArrayList<File> files = new FilesDAO(context).getFileListing(ip, path, false);

        // order and display files
        if(files != null)
            Collections.sort(files, new RegularFileComparator());
        files_recyclerView.setAdapter(new FilesAdapter(files, ip, path, context));

        // check for file splitter folder
        isFileSplitterFolder(files);
    }
}