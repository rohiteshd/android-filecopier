package rdhehe.filecopier.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import rdhehe.filecopier.Beans.StitchFile;
import rdhehe.filecopier.DAO.StitchFilesDAO;
import rdhehe.filecopier.R;
import rdhehe.filecopier.Services.FileService;
import rdhehe.filecopier.Utils.Constants;

public class StitcherFilesAdapter extends RecyclerView.Adapter<StitcherFilesAdapter.ViewHolder> {
    ArrayList<StitchFile> files;
    String ip, path;
    Context context;

    // constructor
    public StitcherFilesAdapter(ArrayList<StitchFile> files, String ip, String path, Context context){
        this.files = files;
        this.ip = ip;
        this.path = path;
        this.context = context;
    }

    // set files
    public void setFiles(ArrayList<StitchFile> files) {
        this.files = files;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.file, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final StitchFile file = files.get(position);

        // set file name and hide connect button
        holder.fileName_textview.setText(file.getFileName());
        holder.connect_button.setVisibility(View.GONE);

        // change text color based on file status
        switch(file.getStatus()){
            case StitchFile.FILESTATUS_NEW:
                holder.fileName_textview.setTextColor(context.getColor(R.color.color_stitchFileNew));
                break;

            case StitchFile.FILESTATUS_COPYING:
                holder.fileName_textview.setTextColor(context.getColor(R.color.color_stitchFileCopying));
                break;

            case StitchFile.FILESTATUS_DONE:
                holder.fileName_textview.setTextColor(context.getColor(R.color.color_stitchFileDone));
                break;
        }

        // recopy file if clicked
        holder.file_cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder;

                // create the alert dialog
                alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle("ReCopy File")
                        .setMessage("ReCopy file " + file.getFileName() + "?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i){
                                Intent intent = new Intent(context, FileService.class);

                                // re copy file via file service
                                intent.putExtra("actionId", FileService.ACTION_COPYSINGLEFILE);
                                intent.putExtra("ip", ip);
                                intent.putExtra("path", path);
                                intent.putExtra("fileName", file.getFileName());
                                intent.putExtra("stitchedFileName", file.getStitchedFileName());
                                FileService.enqueueWork(context, FileService.class, Constants.SERVICE_FILES, intent);

                                // update status of the file
                                file.setStatus(StitchFile.FILESTATUS_NEW);
                                new StitchFilesDAO(ip, path, context).updateCopyStatus(file);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        })
                        .create().show();
            }
        });
    }

    @Override
    public int getItemCount() {
        int count = files!=null ? files.size() : 0;

        return count;
    }



    // view holder class
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView fileName_textview;
        Button connect_button;
        CardView file_cardView;

        public ViewHolder(View itemView) {
            super(itemView);

            fileName_textview = (TextView)itemView.findViewById(R.id.fileName);
            connect_button = (Button)itemView.findViewById(R.id.connectButton);
            file_cardView = (CardView)itemView.findViewById(R.id.file_cardview);
        }
    }
}