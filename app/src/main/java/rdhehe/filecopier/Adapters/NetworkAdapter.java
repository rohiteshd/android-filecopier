package rdhehe.filecopier.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import rdhehe.filecopier.Activities.FilesActivity;
import rdhehe.filecopier.Beans.Network;
import rdhehe.filecopier.R;

public class NetworkAdapter extends RecyclerView.Adapter<NetworkAdapter.ViewHolder> {
    ArrayList<Network> networks;
    Context context;

    // constructor
    public NetworkAdapter(ArrayList<Network> networks, Context context){
        this.networks = networks;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.network, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Network network = networks.get(position);

        // set IP and shared folders
        holder.ip_textview.setText(network.getIp());
        holder.sharedFolders_textView.setText(network.getSharedFiles());

        // show connect button if status is active
        if(network.isStatus())
            holder.connect_button.setVisibility(View.VISIBLE);
        else
            holder.connect_button.setVisibility(View.GONE);

        // set connect button onclick
        holder.connect_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, FilesActivity.class);

                // show files
                intent.putExtra("ip", network.getIp());
                intent.putExtra("path", "");
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        int count = networks!=null ? networks.size() : 0;

        return count;
    }



    // view holder class
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView ip_textview, sharedFolders_textView;
        Button connect_button;

        public ViewHolder(View itemView) {
            super(itemView);

            ip_textview = (TextView)itemView.findViewById(R.id.networkIp);
            sharedFolders_textView = (TextView)itemView.findViewById(R.id.sharedFolders);
            connect_button = (Button)itemView.findViewById(R.id.connectButton);
        }
    }
}