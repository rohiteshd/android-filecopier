package rdhehe.filecopier.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import rdhehe.filecopier.Beans.File;
import rdhehe.filecopier.R;
import rdhehe.filecopier.Services.NetworkService;
import rdhehe.filecopier.Utils.Constants;

public class FilesAdapter extends RecyclerView.Adapter<FilesAdapter.ViewHolder> {
    ArrayList<File> files;
    String ip, path;
    Context context;

    // constructor
    public FilesAdapter(ArrayList<File> files, String ip, String path, Context context){
        this.files = files;
        this.ip = ip;
        this.path = path;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.file, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final File file = files.get(position);

        // set file name
        holder.fileName_textview.setText(file.getFileName());

        // show connect button if folder
        if(file.getFileType() == File.FILETYPE_FOLDER)
            holder.connect_button.setVisibility(View.VISIBLE);
        else
            holder.connect_button.setVisibility(View.GONE);

        // set connect button onclick
        holder.connect_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, NetworkService.class);
                String pathNew = path + "/" + file.getFileName();

                // update path and call network service
                intent.putExtra("actionId", NetworkService.ACTION_DISCOVERFOLDERS);
                intent.putExtra("ip", ip);
                intent.putExtra("path", pathNew);
                NetworkService.enqueueWork(context, NetworkService.class, Constants.SERVICE_NETWORK, intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        int count = files!=null ? files.size() : 0;

        return count;
    }



    // view holder class
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView fileName_textview;
        Button connect_button;

        public ViewHolder(View itemView) {
            super(itemView);

            fileName_textview = (TextView)itemView.findViewById(R.id.fileName);
            connect_button = (Button)itemView.findViewById(R.id.connectButton);
        }
    }
}