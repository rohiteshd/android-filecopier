package rdhehe.filecopier.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import rdhehe.filecopier.Beans.File;
import rdhehe.filecopier.Utils.HelperUtils;
import rdhehe.filecopier.Utils.StringUtils;

public class FilesDAO {
    MainDAO DAOObj;
    Context context;

    // constructor
    public FilesDAO(Context context){
        this.DAOObj = MainDAO.getDAOObj(context);
        this.context = context;
    }

    // get file listing in ip path
    public ArrayList<File> getFileListing(String ip, String path, boolean isOnlyFolders){
        ArrayList<File> fileList = null;

        // dB related
        SQLiteDatabase dB = DAOObj.getWritableDatabase();
        Cursor cursor;

        try {
            dB.beginTransaction();
            cursor = dB.query(MainDAO.tableName_files, new String[]{"ip", "path", "fileName", "fileType"},
                    "ip=?" + (StringUtils.isValid(path) ? " AND path=?" : ""),
                    StringUtils.isValid(path) ? new String[]{ip, path} : new String[]{ip},
                    null, null, null);
            while(cursor.moveToNext()){
                // instantiate arraylist
                if(fileList == null)
                    fileList = new ArrayList<File>();
                fileList.add(new File(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3)));
            }

            dB.setTransactionSuccessful();
        } catch(Exception e){
            HelperUtils.log("Exception : FilesDAO ;: getFileListing()");
        } finally {
            dB.endTransaction();
        }

        // filter folders if flag set
        for(int x=0; isOnlyFolders && fileList!=null && x<fileList.size(); x++)
            if(fileList.get(x).getFileType() != File.FILETYPE_FOLDER)
                fileList.remove(x--);

        return fileList;
    }

    // delete all files of an ip
    public void clearFilesOfIP(String ip){
        // dB related
        SQLiteDatabase dB = DAOObj.getWritableDatabase();

        try {
            dB.beginTransaction();
            dB.delete(MainDAO.tableName_files, "ip=?", new String[]{ip});
            dB.setTransactionSuccessful();
        } catch(Exception e){
            HelperUtils.log("Exception :: FilesDAO :: clearFilesOfIP()");
        } finally {
            dB.endTransaction();
        }
    }

    // insert file
    public void insertFile(String ip, String path, String fileName, int fileType){
        // dB related
        SQLiteDatabase dB = DAOObj.getWritableDatabase();
        ContentValues contentValue = new ContentValues();

        try {
            dB.beginTransaction();

            // form content value and insert into dB
            contentValue.put("ip", ip);
            contentValue.put("path", path);
            contentValue.put("fileName", fileName);
            contentValue.put("fileType", fileType);
            dB.insert(MainDAO.tableName_files, null, contentValue);

            dB.setTransactionSuccessful();
        } catch(Exception e){
            HelperUtils.log("Exception :: FilesDAO :: insertFile()");
        } finally {
            dB.endTransaction();
        }
    }
}