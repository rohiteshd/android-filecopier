package rdhehe.filecopier.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import rdhehe.filecopier.Beans.StitchFile;
import rdhehe.filecopier.Utils.HelperUtils;

public class StitchFilesDAO {
    MainDAO DAOObj;
    Context context;
    String ip, path;

    // constructor
    public StitchFilesDAO(String ip, String path, Context context){
        this.DAOObj = MainDAO.getDAOObj(context);
        this.ip = ip;
        this.path = path;
        this.context = context;
    }

    // update stitched status
    public void updateLocationStitchStatus(boolean isStitched){
        boolean isUpdateOrInsert;

        // dB related
        SQLiteDatabase dB = DAOObj.getWritableDatabase();
        ContentValues contentValue = new ContentValues();
        Cursor cursor;

        try {
            dB.beginTransaction();

            // check if this location exists
            cursor = dB.query(MainDAO.tableName_stitchFiles, new String[]{"isStitched"}, "ip=? AND path=?", new String[]{ip, path}, null, null, null);
            isUpdateOrInsert = cursor.moveToNext();

            // update or insert
            contentValue.put("isStitched", isStitched ? 1 : 0);
            if(isUpdateOrInsert)
                dB.update(MainDAO.tableName_stitchFiles, contentValue, "ip=? AND path=?", new String[]{ip, path});
            else {
                contentValue.put("ip", ip);
                contentValue.put("path", path);
                dB.insert(MainDAO.tableName_stitchFiles, null, contentValue);
            }

            dB.setTransactionSuccessful();
        } catch(Exception e){
            HelperUtils.log("Exception :: StitchFilesDAO :: updateLocationStitchStatus()");
        } finally {
            dB.endTransaction();
        }
    }


    // check if a location is stitched already
    public boolean isLocationStitched(){
        boolean isStitched = false;

        // dB related
        SQLiteDatabase dB = DAOObj.getWritableDatabase();
        Cursor cursor;

        try {
            dB.beginTransaction();
            cursor = dB.query(MainDAO.tableName_stitchFiles, new String[]{"isStitched"}, "ip=? AND path=?", new String[]{ip, path}, null, null, null);
            if(cursor.moveToNext())
                isStitched = cursor.getInt(0)==1 ? true : false;
            dB.setTransactionSuccessful();
        } catch(Exception e){
            HelperUtils.log("Exception :: StitchFilesDAO :: isStitched()");
        } finally {
            dB.endTransaction();
        }

        return isStitched;
    }

    // delete split files after files have been stitched
    /* public void deleteSet(){
        // dB related
        SQLiteDatabase dB = DAOObj.getWritableDatabase();

        try {
            dB.beginTransaction();
            dB.delete(MainDAO.tableName_splitFiles, "ip=? AND path=?", new String[]{ip, path});
            dB.setTransactionSuccessful();
        } catch(Exception e){
            HelperUtils.log("Exception :: StitchFilesDAO :: deleteSet()");
        } finally {
            dB.endTransaction();
        }
    }
    */

    // reset copying files to new when service stops
    public void resetCopyingFilesToNew(){
        // dB related
        SQLiteDatabase dB = DAOObj.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        try {
            dB.beginTransaction();
            contentValues.put("status", StitchFile.FILESTATUS_NEW);
            dB.update(MainDAO.tableName_splitFiles, contentValues, "ip=? AND path=? AND status=?", new String[]{ip, path, Integer.toString(StitchFile.FILESTATUS_COPYING)});
            dB.setTransactionSuccessful();
        } catch(Exception e){
            HelperUtils.log("Exception :: StitchFilesDAO :: resetCopyingFilesToNew()");
        } finally {
            dB.endTransaction();
        }
    }

    // update status
    public void updateCopyStatus(StitchFile stitchFile){
        // dB related
        SQLiteDatabase dB = DAOObj.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        try {
            dB.beginTransaction();
            contentValues.put("status", stitchFile.getStatus());
            dB.update(MainDAO.tableName_splitFiles, contentValues, "ip=? AND path=? AND fileName=?", new String[]{stitchFile.getIp(), stitchFile.getPath(), stitchFile.getFileName()});
            dB.setTransactionSuccessful();
        } catch(Exception e){
            HelperUtils.log("Exception :: StitchFilesDAO :: updateCopyStatus()");
        } finally {
            dB.endTransaction();
        }
    }

    // add stitch files
    public void insertStitchFiles(ArrayList<StitchFile> stitchFiles){
        // dB related
        SQLiteDatabase dB = DAOObj.getWritableDatabase();
        ContentValues contentValues;

        try {
            dB.beginTransaction();

            // parse through the files
            for(int x=0; stitchFiles!=null && x<stitchFiles.size(); x++){
                contentValues = new ContentValues();
                contentValues.put("ip", stitchFiles.get(x).getIp());
                contentValues.put("path", stitchFiles.get(x).getPath());
                contentValues.put("fileName", stitchFiles.get(x).getFileName());
                contentValues.put("stitchedFileName", stitchFiles.get(x).getStitchedFileName());
                contentValues.put("status", stitchFiles.get(x).getStatus());
                dB.insert(MainDAO.tableName_splitFiles, null, contentValues);
            }

            dB.setTransactionSuccessful();
        } catch(Exception e){
            HelperUtils.log("Exception :: StitchFilesDAO :: insertStitchFiles()");
        } finally {
            dB.endTransaction();
        }
    }

    // get stitch files
    public ArrayList<StitchFile> getStitchFiles(){
        ArrayList<StitchFile> stitchFiles = null;

        // dB related
        SQLiteDatabase dB = DAOObj.getWritableDatabase();
        Cursor cursor;

        try {
            dB.beginTransaction();
            cursor = dB.query(MainDAO.tableName_splitFiles, new String[]{"ip", "path", "fileName", "status", "stitchedFileName"}, "ip=? AND path=?", new String[]{ip, path}, null, null, null);
            while(cursor.moveToNext()){
                // instantiate array
                if(stitchFiles == null)
                    stitchFiles = new ArrayList<StitchFile>();
                stitchFiles.add(new StitchFile(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(4), cursor.getInt(3)));
            }
            dB.setTransactionSuccessful();
        } catch(Exception e){
            HelperUtils.log("Exception :: StitchFilesDAO :: getStitchFiles()");
        } finally {
            dB.endTransaction();
        }

        return stitchFiles;
    }
}