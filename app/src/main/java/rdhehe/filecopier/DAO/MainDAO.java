package rdhehe.filecopier.DAO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

// architecture inspired by : http://touchlabblog.tumblr.com/post/24474750219/single-sqlite-connection
public class MainDAO extends SQLiteOpenHelper {
    protected static final String DB_NAME = "filecopier.db";
	protected static final int DB_VERSION = 1;
	
	// dB related
	private static MainDAO DAOObj = null;
	
	// table names
	public static String tableName_files = "files";
	public static String tableName_network = "network";
	public static String tableName_splitFiles = "splitfiles";
	public static String tableName_stitchFiles = "stitchfiles";

	// singleton implementation
	public static synchronized MainDAO getDAOObj(Context context){
		// instantiate the DAO if not already done
		if(DAOObj == null)
			DAOObj = new MainDAO(context.getApplicationContext());

		return DAOObj;
	}

	// parameterized constructor
	private MainDAO(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase dB) {
		try {
			dB.beginTransaction();

			// create the tables
			dB.execSQL("CREATE TABLE IF NOT EXISTS " + tableName_files + " (ip TEXT, path TEXT, fileName TEXT, fileType INTEGER)");
			dB.execSQL("CREATE TABLE IF NOT EXISTS " + tableName_network + " (ip TEXT, sharedFiles TEXT, status BOOLEAN)");
			dB.execSQL("CREATE TABLE IF NOT EXISTS " + tableName_splitFiles + " (ip TEXT, path TEXT, fileName TEXT, status BOOLEAN, stitchedFileName TEXT)");
			dB.execSQL("CREATE TABLE IF NOT EXISTS " + tableName_stitchFiles + " (ip TEXT, path TEXT, isStitched BOOLEAN)");

            // indices for the tables
            // dB.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS unique_errors ON " + tableName_errors + " (comment)");

			dB.setTransactionSuccessful();
		} catch (Exception e){
			// do nothing
		} finally {
			dB.endTransaction();
		}
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase dB, int oldVersion, int newVersion) {
		try {
            dB.beginTransaction();

			// drop the table if exists
			dB.execSQL("DROP TABLE IF EXISTS " + tableName_files);
			dB.execSQL("DROP TABLE IF EXISTS " + tableName_network);
			dB.execSQL("DROP TABLE IF EXISTS " + tableName_splitFiles);
			dB.execSQL("DROP TABLE IF EXISTS " + tableName_stitchFiles);

            dB.setTransactionSuccessful();
		} catch(Exception e){
			// do nothing
		} finally {
			dB.endTransaction();
		}
		
		// create everything afresh
		onCreate(dB);
	}
}