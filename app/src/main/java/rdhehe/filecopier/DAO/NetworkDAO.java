package rdhehe.filecopier.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashMap;

import rdhehe.filecopier.Beans.File;
import rdhehe.filecopier.Beans.Network;
import rdhehe.filecopier.Utils.HelperUtils;
import rdhehe.filecopier.Utils.StringUtils;

public class NetworkDAO {
	Context context;
	MainDAO DAOObj;

	public NetworkDAO(Context context) {
        this.DAOObj = MainDAO.getDAOObj(context);
        this.context = context;
	}

	// get networks
    public ArrayList<Network> getNetworks(boolean isOnlyReachable){
	    ArrayList<Network> networks = null;
	    Network network;

	    // dB related
        Cursor cursor;
        SQLiteDatabase dB = DAOObj.getWritableDatabase();

        try {
            dB.beginTransaction();
            cursor = dB.query(MainDAO.tableName_network, new String[]{"ip", "sharedFiles", "status"},
                    isOnlyReachable ? "status=?" : null,
                    isOnlyReachable ? new String[]{"1"} : null,
                    null, null, null);
            while(cursor.moveToNext()){
                network = new Network(cursor.getString(0), cursor.getString(1), cursor.getInt(2)!=0);

                // add to array
                if(networks == null)
                    networks = new ArrayList<Network>();
                networks.add(network);
            }

            dB.setTransactionSuccessful();
        } catch(Exception e){
            HelperUtils.log("Exception :: NetworkDAO :: getNetworks()");
        } finally {
            dB.endTransaction();
        }

	    return networks;
    }

    // reset the status of all ol networks
    public void resetAllNetworks(){
	    // dB related
        SQLiteDatabase dB = DAOObj.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        try {
            dB.beginTransaction();
            contentValues.put("status", 0);
            dB.update(MainDAO.tableName_network, contentValues, null, null);
            dB.setTransactionSuccessful();
        } catch(Exception e){
            HelperUtils.log("Exception :: NetworkDAO :: resetAllNetworks()");
        } finally {
            dB.endTransaction();
        }
    }

    // update network status
    public void updateStatus(String ip, ArrayList<File> sharedFiles, boolean status){
	    String sharedFilesStr = "";

	    // dB related
        SQLiteDatabase dB = DAOObj.getWritableDatabase();
        ContentValues contentValue = new ContentValues();

        // parse through shared files and form string if name doesn't contain $ (to ignore print$ etc)
        for(int x=0; sharedFiles!=null && x<sharedFiles.size(); x++)
            if(!sharedFiles.get(x).getFileName().contains("$")){
                sharedFilesStr += StringUtils.isValid(sharedFilesStr) ? ", " : "";
                sharedFilesStr += sharedFiles.get(x).getFileName();
            }

        try {
            dB.beginTransaction();

            // create content values
            contentValue.put("status", status ? 1 : 0);
            contentValue.put("sharedFiles", sharedFilesStr);

            // upate in dB
            dB.update(MainDAO.tableName_network, contentValue, "ip=?", new String[]{ip});

            dB.setTransactionSuccessful();
        } catch(Exception e){
            HelperUtils.log("Exception :: NetworkDAO :: updateCopyStatus()");
        } finally {
            dB.endTransaction();
        }
    }

    // insert network
    public void insertNetwork(String ip, ArrayList<File> sharedFiles){
	    String sharedFilesStr = "";
        HashMap<String, String> networks_hash = new HashMap<String, String>();
        ArrayList<Network> networks_arr = getNetworks(false);

	    // dB related
        SQLiteDatabase dB = DAOObj.getWritableDatabase();
        ContentValues contentValue;

        // form hashmap
        for(int x=0; networks_arr!=null && x<networks_arr.size(); x++)
            networks_hash.put(networks_arr.get(x).getIp(), "");

        // parse through shared files and form string if name doesn't contain $ (to ignore print$ etc)
        for(int x=0; sharedFiles!=null && x<sharedFiles.size(); x++)
            if(!sharedFiles.get(x).getFileName().contains("$")){
                sharedFilesStr += StringUtils.isValid(sharedFilesStr) ? ", " : "";
                sharedFilesStr += sharedFiles.get(x).getFileName();
            }

        try {
            dB.beginTransaction();

            // insert if not present
            if(!networks_hash.containsKey(ip)) {
                contentValue = new ContentValues();
                contentValue.put("ip", ip);
                contentValue.put("sharedFiles", sharedFilesStr);
                contentValue.put("status", 1);
                dB.insert(MainDAO.tableName_network, null, contentValue);
            }

            dB.setTransactionSuccessful();
        } catch(Exception e){
            HelperUtils.log("Exception :: NetworkDAO :: insertNetwork()");
        } finally {
            dB.endTransaction();
        }
    }
}